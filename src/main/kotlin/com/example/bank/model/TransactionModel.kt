package com.example.bank.model

import lombok.Data
import java.util.Date


@Data
class TransactionModel(
    val targetAccount: String,
    val amount: Double,
    val description: String,
)

fun TransactionModel.convertToTransactionModel() = TransactionDBModel(
    accountIdentifier = this.targetAccount,
    amount = this.amount,
    description = this.description
)