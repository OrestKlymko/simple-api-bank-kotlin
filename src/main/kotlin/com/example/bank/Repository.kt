package com.example.bank

import com.example.bank.model.TransactionDBModel
import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface Repository:CrudRepository<TransactionDBModel,UUID>
