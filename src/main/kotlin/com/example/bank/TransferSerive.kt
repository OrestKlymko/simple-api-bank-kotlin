package com.example.bank

import com.example.bank.model.OverviewTransactionModel
import com.example.bank.model.TransactionDBModel
import com.example.bank.model.convertToOverviewTransactionModel
import com.example.bank.model.convertToTransactionModel
import org.springframework.stereotype.Service

@Service
class TransferSerive(val repository: Repository) {

    fun findAllTransaction(): List<OverviewTransactionModel> {
        return repository.findAll().map { it -> it.convertToOverviewTransactionModel() }
    }

    fun saveNewTransaction(transactionDBModel: TransactionDBModel) {
        repository.save(transactionDBModel);
    }
}