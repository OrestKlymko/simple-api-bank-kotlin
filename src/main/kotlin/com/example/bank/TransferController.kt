package com.example.bank

import com.example.bank.model.OverviewTransactionModel
import com.example.bank.model.TransactionDBModel
import com.example.bank.model.TransactionModel
import com.example.bank.model.convertToTransactionModel
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/transfer")
class TransferController(val service: TransferSerive) {


    @PostMapping("/new")
    fun newTransfer(@RequestBody transactionModel: TransactionModel) {
        service.saveNewTransaction(transactionDBModel = transactionModel.convertToTransactionModel());
    }

    @GetMapping
    fun getAllTransfer(): List<OverviewTransactionModel> {
        return service.findAllTransaction();
    }
}